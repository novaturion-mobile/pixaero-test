import 'package:flutter/material.dart';
import 'package:pixaero_test/app/widgets/widgets.dart';
import 'package:pixaero_test/models/models.dart';

class CombinedTemperatureWidget extends StatelessWidget {
  final WeatherModel weather;

  CombinedTemperatureWidget({
    Key key,
    @required this.weather,
  })  : assert(weather != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(20.0),
              child: WeatherConditionsWidget(condition: weather.condition),
            ),
            Padding(
              padding: EdgeInsets.all(20.0),
              child: TemperatureWidget(
                temperature: weather.temp,
                high: weather.maxTemp,
                low: weather.minTemp,
              ),
            ),
          ],
        ),
        Center(
          child: Text(
            weather.formattedCondition,
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w200,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
