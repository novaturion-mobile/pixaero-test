class WeatherApiKeys {
  static const baseUrl = 'https://www.metaweather.com/';
  static const locationUrl = 'api/location/';
  static const locationSearchUrl = 'api/location/search/?query=';
}
