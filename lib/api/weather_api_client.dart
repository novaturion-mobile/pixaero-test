import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

import 'api_keys.dart';

class WeatherApiClient {
  final http.Client httpClient;

  WeatherApiClient({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<http.Response> getLocationId(String city) async {
    final locationUrl = '${WeatherApiKeys.baseUrl}${WeatherApiKeys.locationSearchUrl}$city';
    return await this.httpClient.get(locationUrl);
  }

  Future<http.Response> fetchWeather(int locationId) async {
    final weatherUrl = '${WeatherApiKeys.baseUrl}${WeatherApiKeys.locationUrl}$locationId';
    return await this.httpClient.get(weatherUrl);
  }
}
