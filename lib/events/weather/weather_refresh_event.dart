import 'package:meta/meta.dart';

import './weather_event.dart';

class WeatherRefreshEvent extends WeatherEvent {
  final String city;

  const WeatherRefreshEvent({@required this.city}) : assert(city != null);

  @override
  List<Object> get props => [city];
}
