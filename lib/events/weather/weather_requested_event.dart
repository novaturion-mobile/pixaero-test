import 'package:meta/meta.dart';

import './weather_event.dart';

class WeatherRequestedEvent extends WeatherEvent {
  final String city;

  const WeatherRequestedEvent({@required this.city}) : assert(city != null);

  @override
  List<Object> get props => [city];
}
