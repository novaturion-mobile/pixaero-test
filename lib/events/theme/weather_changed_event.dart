import 'package:meta/meta.dart';
import 'package:pixaero_test/models/models.dart';

import 'theme_event.dart';

class WeatherChangedEvent extends ThemeEvent {
  final WeatherCondition condition;

  const WeatherChangedEvent({@required this.condition})
      : assert(condition != null);

  @override
  List<Object> get props => [condition];
}
