import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pixaero_test/models/models.dart';

class WeatherParser {
  static Future<int> parseLocation(http.Response response) async {
    if (response.statusCode != 200) {
      throw Exception('error getting locationId for city');
    }

    final locationJson = jsonDecode(response.body) as List;
    return (locationJson.first)['woeid'];
  }

  static Future<WeatherModel> parseWeather(http.Response response) async {
    if (response.statusCode != 200) {
      throw Exception('error getting weather for location');
    }

    final weatherJson = jsonDecode(response.body);
    return WeatherModel.fromJson(weatherJson);
  }
}
