import 'dart:async';

import 'package:meta/meta.dart';
import 'package:pixaero_test/api/api.dart';
import 'package:pixaero_test/models/models.dart';
import 'package:pixaero_test/parsers/parsers.dart';

class WeatherRepository {
  final WeatherApiClient weatherApiClient;

  WeatherRepository({@required this.weatherApiClient})
      : assert(weatherApiClient != null);

  Future<WeatherModel> getWeather(String city) async {
    final locationResponse = await weatherApiClient.getLocationId(city);
    final locationId = await WeatherParser.parseLocation(locationResponse);

    final weatherResponse = await weatherApiClient.fetchWeather(locationId);
    return WeatherParser.parseWeather(weatherResponse);
  }
}
