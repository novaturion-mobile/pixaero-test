import 'package:equatable/equatable.dart';

enum WeatherCondition {
  unknown,
  snow,
  sleet,
  hail,
  thunderstorm,
  heavyRain,
  lightRain,
  showers,
  heavyCloud,
  lightCloud,
  clear
}

class WeatherModel extends Equatable {
  final int locationId;
  final double minTemp;
  final double temp;
  final double maxTemp;
  final String formattedCondition;
  final String created;
  final String location;
  final DateTime lastUpdated;
  final WeatherCondition condition;

  const WeatherModel({
    this.locationId,
    this.minTemp,
    this.temp,
    this.maxTemp,
    this.formattedCondition,
    this.created,
    this.location,
    this.lastUpdated,
    this.condition,
  });

  @override
  List<Object> get props => [
        locationId,
        minTemp,
        temp,
        maxTemp,
        formattedCondition,
        created,
        location,
        lastUpdated,
        condition,
      ];

  static WeatherModel fromJson(dynamic json) {
    final consolidatedWeather = json['consolidated_weather'][0];
    return WeatherModel(
      condition: _mapStringToWeatherCondition(
          consolidatedWeather['weather_state_abbr']),
      formattedCondition: consolidatedWeather['weather_state_name'],
      minTemp: consolidatedWeather['min_temp'] as double,
      temp: consolidatedWeather['the_temp'] as double,
      maxTemp: consolidatedWeather['max_temp'] as double,
      locationId: json['woeid'] as int,
      created: consolidatedWeather['created'],
      lastUpdated: DateTime.now(),
      location: json['title'],
    );
  }

  static WeatherCondition _mapStringToWeatherCondition(String input) {
    WeatherCondition condition;
    switch (input) {
      case 'sn':
        condition = WeatherCondition.snow;
        break;
      case 'sl':
        condition = WeatherCondition.sleet;
        break;
      case 'h':
        condition = WeatherCondition.hail;
        break;
      case 't':
        condition = WeatherCondition.thunderstorm;
        break;
      case 'hr':
        condition = WeatherCondition.heavyRain;
        break;
      case 'lr':
        condition = WeatherCondition.lightRain;
        break;
      case 's':
        condition = WeatherCondition.showers;
        break;
      case 'hc':
        condition = WeatherCondition.heavyCloud;
        break;
      case 'lc':
        condition = WeatherCondition.lightCloud;
        break;
      case 'c':
        condition = WeatherCondition.clear;
        break;
      default:
        condition = WeatherCondition.unknown;
    }
    return condition;
  }
}
