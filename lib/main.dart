import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:pixaero_test/api/api.dart';
import 'package:pixaero_test/app/app.dart';
import 'package:pixaero_test/blocs/blocs.dart';
import 'package:pixaero_test/observers/observers.dart';
import 'package:pixaero_test/repositories/repositories.dart';

void main() {
  Bloc.observer = WeatherBlocObserver();

  final WeatherRepository weatherRepository = WeatherRepository(
    weatherApiClient: WeatherApiClient(
      httpClient: http.Client(),
    ),
  );

  runApp(
    BlocProvider<ThemeBloc>(
      create: (context) => ThemeBloc(),
      child: App(weatherRepository: weatherRepository),
    ),
  );
}
