import 'package:meta/meta.dart';
import 'package:pixaero_test/models/models.dart';

import './weather_state.dart';

class WeatherLoadSuccessState extends WeatherState {
  final WeatherModel weather;

  const WeatherLoadSuccessState({@required this.weather}) : assert(weather != null);

  @override
  List<Object> get props => [weather];
}
