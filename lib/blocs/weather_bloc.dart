import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:pixaero_test/events/events.dart';
import 'package:pixaero_test/events/weather/weather_refresh_event.dart';
import 'package:pixaero_test/repositories/repositories.dart';
import 'package:pixaero_test/states/states.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepository weatherRepository;

  WeatherBloc({@required this.weatherRepository})
      : assert(weatherRepository != null),
        super(WeatherInitialState());

  @override
  Stream<WeatherState> mapEventToState(WeatherEvent event) async* {
    if (event is WeatherRequestedEvent) {
      yield* _mapWeatherRequestedToState(event);
    } else if (event is WeatherRefreshEvent) {
      yield* _mapWeatherRefreshRequestedToState(event);
    }
  }

  Stream<WeatherState> _mapWeatherRequestedToState(
      WeatherRequestedEvent event) async* {
    yield WeatherLoadingState();
    try {
      final weather = await weatherRepository.getWeather(event.city);
      yield WeatherLoadSuccessState(weather: weather);
    } catch (_) {
      yield WeatherLoadFailureState();
    }
  }

  Stream<WeatherState> _mapWeatherRefreshRequestedToState(
      WeatherRefreshEvent event) async* {
    try {
      final weather = await weatherRepository.getWeather(event.city);
      yield WeatherLoadSuccessState(weather: weather);
    } catch (_) {}
  }
}
